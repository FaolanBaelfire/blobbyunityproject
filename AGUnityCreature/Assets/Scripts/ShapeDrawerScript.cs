﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeDrawerScript : MonoBehaviour
{

    public bool isDrawerOpen = false;
    public float drawerSpeed = .5f;
    public Vector2 localStartPos = new Vector2(0, 0);
    public Vector2 localEndPos = new Vector2(-300f, 0);
    public RectTransform myRectPos;
    


    // Start is called before the first frame update
    void Start()
    {
        myRectPos = GetComponent<RectTransform>();
        localStartPos.y = myRectPos.anchoredPosition.y;
        localEndPos.y = myRectPos.anchoredPosition.y;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GlobalVariables.Instance.curGameMode == GlobalVariables.GameMode.Normal)
        {

            if (isDrawerOpen && myRectPos.anchoredPosition != localEndPos)
            {
                OpenDrawer();
            }
            else if (!isDrawerOpen && myRectPos.anchoredPosition != localStartPos)
            {
                CloseDrawer();
            }
        }
        else
        {
            CloseDrawer();
        }
        
    }
    
    public void ToggleShapeDrawer()
    {
        isDrawerOpen = !isDrawerOpen;

    }

    public void OpenDrawer() {
        myRectPos.anchoredPosition = Vector2.Lerp(myRectPos.anchoredPosition, localEndPos, drawerSpeed);
    }
    public void CloseDrawer()
    {
        myRectPos.anchoredPosition = Vector2.Lerp(myRectPos.anchoredPosition, localStartPos, drawerSpeed);
    }
}
