﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GlobalVariables : MonoBehaviour
{
    public static GlobalVariables Instance { get; private set; }

    public enum GameMode
    {
        Normal,
        CookieRush
    }

    public GameMode curGameMode = GameMode.Normal;

    public int ownedCookies = 0;
    public GameObject cookieText;
    public float cookieRushTimer;
    public GameObject cookieRushTimerText;
    public bool isFedCookieSpawned = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (cookieText != null)
        {
            cookieText.GetComponent<TextMeshProUGUI>().text = "Cookies Held: " + ownedCookies;
        }
        if (curGameMode == GameMode.CookieRush)
        {
            
            cookieRushTimerText.GetComponent<TextMeshProUGUI>().text = Mathf.Floor(cookieRushTimer).ToString();
            cookieRushTimer -= Time.deltaTime;
        }
        else
        {
            cookieRushTimerText.GetComponent<TextMeshProUGUI>().text = "";
            
        }
    }

    public void StartCookieRush()
    {
        curGameMode = GameMode.CookieRush;
        cookieRushTimer = 35f;
    }

    public void EndCookieRush()
    {
        curGameMode = GameMode.Normal;
        cookieRushTimer = 0;
    }
    

}
