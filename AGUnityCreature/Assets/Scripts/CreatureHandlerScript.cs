﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CreatureHandlerScript : MonoBehaviour

{
    //Shapes Blobby can change into
    [Header("Shapes")]
    public List<Sprite> myShapes = new List<Sprite>();
    private string myChangingShape;

    
    [Header("Audio")]
    //Blobby's Sounds
    public AudioClip Bounce;
    public AudioClip ChangeShape;
    public AudioClip ChangeColor;
    private AudioSource myAudioSource;
    

    [Header("Eye Parts")]
    //Blobby's Eyelids and Pupils
    public GameObject LeftPupil;
    public GameObject RightPupil;
    public GameObject LeftEyelid;
    public GameObject RightEyelid;
    public GameObject LeftEye;
    public GameObject RightEye;
    public GameObject RightEyeSquint;
    public GameObject LeftEyeSquint;
    
    //Blobby's pom
    private SpriteRenderer myPom;


    //Polygon Collider points
    private List<Vector2> polyPoints = new List<Vector2>();
    private List<Vector2> simplifiedPolyPoints = new List<Vector2>();

    //Double Click handler variables
    private int TimesClicked = 0;
    private float ClickDelay = .5f;
    private float DoubleClickTime = 0;
    private float IdleClickTime = 0;

    
    //Random Color Gen Variables
    private SpriteRenderer mySpriteRenderer;
    private PolygonCollider2D myPolygonCollider2D;

    //Blobby's animooter
    private Animator myAnimator;



    // Start is called before the first frame update
    void Start()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        myPolygonCollider2D = GetComponent<PolygonCollider2D>();
        myPom = transform.GetChild(1).GetComponent<SpriteRenderer>();
        myAnimator = GetComponent<Animator>();
        myAudioSource = GetComponent<AudioSource>();
        myChangingShape = "circle";
    }

    void OnMouseDown()
    {
        TimesClicked++;
        if (TimesClicked == 1)
        {
            DoubleClickTime = Time.time;
            IdleClickTime += Time.deltaTime;
        }
        if (TimesClicked > 1 && (Time.time - DoubleClickTime) < ClickDelay)
        {
            TimesClicked = 0;
            DoubleClickTime = 0;
            IdleClickTime = 0;
            myAnimator.SetBool("isDoubleClicked", true);
            
        }
        else if (TimesClicked > 2 || Time.time - DoubleClickTime > 1)
        {
            TimesClicked = 0;
        } 
    }

    public void ChangeMyColor()
    {
        Color randomColor = Random.ColorHSV(0f, 1f, 0f, 1f, 1f, 1f);
        mySpriteRenderer.color = randomColor;
        myPom.color = randomColor;
        LeftEyeSquint.GetComponent<SpriteRenderer>().color = randomColor;
        RightEyeSquint.GetComponent<SpriteRenderer>().color = randomColor;
    }

    public void ChangeMyShape()
    {
        switch (myChangingShape)
        {
            case "circle":
                mySpriteRenderer.sprite = myShapes[0];
                myPom.sprite = myShapes[0];
                break;
            case "hexagon":
                mySpriteRenderer.sprite = myShapes[1];
                myPom.sprite = myShapes[1];
                break;
            case "square":
                mySpriteRenderer.sprite = myShapes[2];
                myPom.sprite = myShapes[2];
                break;
            case "diamond":
                mySpriteRenderer.sprite = myShapes[3];
                myPom.sprite = myShapes[3];
                break;
            default:
                break;
        }
        RefreshCollider();
    }

    public void RefreshCollider(float tolerance = .05f)
    {
        for (int i = 0; i < myPolygonCollider2D.pathCount; i++)
        {
            mySpriteRenderer.sprite.GetPhysicsShape(i, polyPoints);
            LineUtility.Simplify(polyPoints, tolerance, simplifiedPolyPoints);
            myPolygonCollider2D.SetPath(i, simplifiedPolyPoints);
        }
        
    }

    public void ResetDoubleClickedAnim()
    {
        myAnimator.SetBool("isDoubleClicked", false);
    }

    public void ResetChangeShapeAnim() {
        myAnimator.SetBool("isChangingShape", false);
    }
    public void StartChangeShapeAnim(string shape)
    {
        myAnimator.SetBool("isChangingShape", true);
        myChangingShape = shape;
    }

    public void ChangeEyeState (string eyeState)
    {
        switch (eyeState)
        {
            case "normal":
                EnableEyes();
                LeftEyelid.GetComponent<SpriteMask>().enabled = false;
                RightEyelid.GetComponent<SpriteMask>().enabled = false;
                LeftPupil.transform.localPosition = new Vector2(0, 0);
                RightPupil.transform.localPosition = new Vector2(0, 0);
                break;
            case "sad":
                EnableEyes();
                LeftEyelid.GetComponent<SpriteMask>().enabled = true;
                RightEyelid.GetComponent<SpriteMask>().enabled = true;
                LeftEyelid.transform.localPosition = new Vector2(-.05f, .15f);
                RightEyelid.transform.localPosition = new Vector2(.05f, .15f);
                LeftPupil.transform.localPosition = new Vector2(0, -.05f);
                RightPupil.transform.localPosition = new Vector2(0, -.05f);
                break;
            case "happy":
                EnableHappyEyes();
                break;
            default:
                break;
        }

    }

    public void EnableEyes()
    {
        LeftEyeSquint.GetComponent<SpriteRenderer>().enabled = false;
        RightEyeSquint.GetComponent<SpriteRenderer>().enabled = false;
        LeftPupil.GetComponent<SpriteRenderer>().enabled = true;
        RightPupil.GetComponent<SpriteRenderer>().enabled = true;
        LeftEye.GetComponent<SpriteRenderer>().enabled = true;
        RightEye.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void EnableHappyEyes()
    {
            LeftEyeSquint.GetComponent<SpriteRenderer>().enabled = true;
            RightEyeSquint.GetComponent<SpriteRenderer>().enabled = true;
            LeftEyelid.GetComponent<SpriteMask>().enabled = false;
            RightEyelid.GetComponent<SpriteMask>().enabled = false;
            LeftPupil.GetComponent<SpriteRenderer>().enabled = false;
            RightPupil.GetComponent<SpriteRenderer>().enabled = false;
        LeftEye.GetComponent<SpriteRenderer>().enabled = false;
            RightEye.GetComponent<SpriteRenderer>().enabled = false;
            LeftEyeSquint.GetComponent<SpriteRenderer>().color = mySpriteRenderer.color;
            RightEyeSquint.GetComponent<SpriteRenderer>().color = mySpriteRenderer.color;
    }

    public void FeedHim()
    {
        myAnimator.SetBool("isFed", true);
    }
    public void ResetFeed()
    {
        myAnimator.SetBool("isFed", false);
    }

    public void StartCookieRush()
    {
        myPolygonCollider2D.enabled = false;
    }

    public void EndCookieRush()
    {        
        myPolygonCollider2D.enabled = true;
    }

    private void Update()
    {
        if (TimesClicked == 1)
        {
            IdleClickTime += Time.deltaTime;

            if(IdleClickTime >= 1f)
            {
                TimesClicked = 0;
                IdleClickTime = 0;
            }
        }


        if(GlobalVariables.Instance.curGameMode == GlobalVariables.GameMode.CookieRush)
        {
            StartCookieRush();
        }
        else
        {
            if(myPolygonCollider2D.enabled == false && GlobalVariables.Instance.curGameMode == GlobalVariables.GameMode.Normal)
            {
                EndCookieRush();
            }
            
        }
    }

    public void PlaySound(string soundName)
    {
        switch (soundName)
        {
            case "bounce":
                myAudioSource.clip = Bounce;
                myAudioSource.Play();
                break;
            case "changeColor":
                myAudioSource.clip = ChangeColor; 
                myAudioSource.Play();
                break;
            case "transform":
                myAudioSource.clip = ChangeShape;
                myAudioSource.Play();
                break;
            default:
                break;

        }
    }
}
