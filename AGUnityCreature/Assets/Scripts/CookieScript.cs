﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookieScript : MonoBehaviour
{
    public Rigidbody2D myRigidbody;
    public bool isBeingDragged = false;
    public bool isDissolving = false;
    public Material myMaterial;
    public float fadeTime = 1f;
    public Transform BlobbysTransform;
    

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        myMaterial = GetComponent<SpriteRenderer>().material;
     }

    // Update is called once per frame
    void Update()
    {
        if (isBeingDragged)
        {
            Vector2 mousePos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            myRigidbody.MovePosition(mousePos);
        }
        if (isDissolving)
        {
            fadeTime -= Time.deltaTime;
            myMaterial.SetFloat("_Fade", fadeTime);
            Vector2 absorbVect = Vector2.Lerp((Vector2)BlobbysTransform.position, myRigidbody.position, .8f);
            myRigidbody.MovePosition(absorbVect);
        }
        if (GlobalVariables.Instance.curGameMode == GlobalVariables.GameMode.CookieRush)
        {
            CleanUp();
        }
    }

    private void OnMouseDown()
    {
        if (!isDissolving && GlobalVariables.Instance.curGameMode == GlobalVariables.GameMode.Normal)
        {
            isBeingDragged = true;
        }
    }

    private void OnMouseUp()
    {
        isBeingDragged = false;  
    }

    public void OnMouseEnter()
    {
        if(GlobalVariables.Instance.curGameMode == GlobalVariables.GameMode.CookieRush)
        {
            GlobalVariables.Instance.ownedCookies++;
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if(collision.transform.name == "Blobby")
        {
            isDissolving = true;
            isBeingDragged = false;
            GetComponent<Collider2D>().enabled = false;
            BlobbysTransform = collision.transform;
            BlobbysTransform.GetComponent<CreatureHandlerScript>().FeedHim();
            CleanUp();
        }
    } 
    
    private void CleanUp()
    {
        GlobalVariables.Instance.isFedCookieSpawned = false;
        Destroy(gameObject, 5f);
    }


}
