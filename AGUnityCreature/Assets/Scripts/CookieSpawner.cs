﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookieSpawner : MonoBehaviour
{
    public GameObject spawnerBeginObj;
    public GameObject spawnerEndObj;

    public Vector2 spawnerBeginPos;
    public Vector2 spawnerEndPos;
    public float spawnTime;
    public float curSpawnTime;
    

    public GameObject cookiePrefab;
    public int cookieCount = 0;

    private void Start()
    {
        curSpawnTime = spawnTime;
        spawnerBeginPos = (Vector2)spawnerBeginObj.transform.position;
        spawnerEndPos = (Vector2)spawnerEndObj.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(GlobalVariables.Instance.curGameMode == GlobalVariables.GameMode.CookieRush)
        {

            curSpawnTime -= Time.deltaTime;
            if(cookieCount < 30 && curSpawnTime <=0)
            {
                cookieCount++;
                Vector3 newSpawnPos = new Vector3(Random.Range(spawnerBeginPos.x, spawnerEndPos.y), spawnerBeginPos.y, 0);
                GameObject tempCookie = Instantiate(cookiePrefab, newSpawnPos, cookiePrefab.transform.rotation);
                tempCookie.GetComponent<Rigidbody2D>().gravityScale = Random.Range(.5f, 3f);
                curSpawnTime = spawnTime;
                tempCookie.GetComponent<SpriteRenderer>().sortingOrder = 5;
            }
            else if (cookieCount >= 30)
            {
                StartCoroutine(EndCookieRush());

                
            }
        }
    }

    public IEnumerator EndCookieRush()
    {
        yield return new WaitForSeconds(3f);
        GlobalVariables.Instance.curGameMode = GlobalVariables.GameMode.Normal;
        curSpawnTime = spawnTime;
        cookieCount = 0;
    }

    public void SpawnCookieToFeed()
    {
        if (GlobalVariables.Instance.curGameMode == GlobalVariables.GameMode.Normal)
        {
            if(GlobalVariables.Instance.ownedCookies > 0 && GlobalVariables.Instance.isFedCookieSpawned == false)
            {
                GlobalVariables.Instance.isFedCookieSpawned = true;
                Instantiate(cookiePrefab, new Vector3(0, 0, 0), cookiePrefab.transform.rotation);
                GlobalVariables.Instance.ownedCookies--;
            }
        }
    }
}
